As technology develops it becomes more and more available to people all around the globe. However, in some cases with blind or visually impaired users, this technology is not as accessible as it is for sighted users. For instance, text input for computer systems can pose a problem for less-sighted users as the standard keyboard is difficult to use without full sight. Moreover, existing solutions for blind users can be costly.


This paper proposes a low-cost and readily accessible approach to teach blind and visually impaired people to use standard text-input methods. The proposed solution is a web application using spatial-to-audio keyboard mapping through binaural recording, and a set of Braille keycap adhesives.

Paper: https://goo.gl/u64zXm